using System;
using System.Reflection;

namespace LVL2_ASPNet_MVC_04x.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}